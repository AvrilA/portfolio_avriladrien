<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="{{ url('css/style.css') }}" rel="stylesheet" type="text/css">
        <!-- Styles -->
    </head>
    <body>

        <header>
            
             <img class="logo-adrien"
                 src="{{url('media/logo-adrien.png')}}"
                 alt="computer on descktop" />
                 
                 @if (Auth::user() && Auth::user()->rank === 1)
                 <a href="/backoff"> <img class="logo-adrien-back"
                 src="{{url('media/logo-adrien.png')}}"
                 alt="computer on descktop" />
                 </a>
                  @endif
                 
            <p id="txt-1">‘’</p> 
             
             <p id="txt-2">‘’</p>     
                 
                  <p id="txt-header">On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme Du texte. </p>
                  
                   
    
            <div id="line">



                <div class="container">

                    <div class="line1"></div>
                    <div class="line2"></div> 
                    <div class="line3"></div> 
                    <div class="line4"></div>
                    <div class="line5"></div>
                    <div class="line6"></div> 
                    <div class="line7"></div> 
                    <div class="line8"></div>   

                </div>

                <div id="menu">

                    <a href="#part1" class="voice">PRESENTATION </a>


                    <div class="container2">
                        <div class="line9"></div> 
                    </div>

                    <a href="#part3" class="voice">SITES </a>

                    <div class="container2">
                        <div class="line10"></div> 
                    </div>

                    <a href="#part4" class="voice">CONTACT </a>

                </div>
            </div>

            <div id="trapezoid">



            </div>

            <div id="trapezoid2">



            </div>

            <img class="fit-picture"
                 src="{{url('media/top_img.jpg')}}"
                 alt="computer on descktop" />

        </header>


        

        <!--        SELECT `img` FROM `backoffice` WHERE id= '2' ;-->

        <section id="part1">
            
            <p id="txt-part1-left">On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme Du texte. </p>
            
            <p id="txt-part1-right">On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme Du texte. </p>
            
            
            

            <div class="line-center"></div>
            <img class="logo-adrien2"
                 src="{{url('media/logo-adrien.png')}}"
                 alt="computer on descktop" />



        </section>



        <section id="part2">



            <div id="slider">
                <figure>
                    <img  src="{{url('media/transfiles.jpg')}}" alt>
                    <img  src="{{url('media/modaba.jpg')}}" alt>
                    <img  src="{{url('media/never-become.jpg')}}" alt>
                    <img  src="{{url('media/puissance4.jpg')}}" alt>

                </figure>

                <div id="left">

                    <div class="left"></div>  

                </div>

                <div id="right">

                    <div class="right"></div>  

                </div>
            </div>



        </section>


        <section id="part3">
            

            <div class="logo"><img class="trans" src="{{url('/media/TSmini.png')}}" alt></div>
            <div class="logo"><img class="modaba" src="{{url('/media/logo.png')}}" alt></div>
            <div class="logo"><img class="become" src="{{url('/media/wordpress.png')}}" alt></div>
            <div class="logo"><img class="puiss" src="{{url('/media/glass-r.png')}}" alt></div>

        </section>


        <section id="part4">


            <form id="form1" method="post" action="recup.php">


                <input class="nom" type="text" placeholder="Nom" name="nom" >

                <input class="email"  type="text" placeholder="Email" name="email" >

                <textarea class="message"  type="text" placeholder="Message" name="message" ></textarea>

                <input id="submit" type="submit" name="submit" value="envoyer">

            </form>

            <p id="txt-form">On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme Du texte. </p>

        </section>

        <section id="footer">



            <img class="logo-adrien-bas"
                 src="{{ url('/media/logo-adrien.png')}}"
                 alt="computer on descktop" />




            <div id="line-bas">

                <div class="container-bas">

                    <div class="line1-bas"></div>
                    <div class="line2-bas"></div> 
                    <div class="line3-bas"></div> 
                    <div class="line4-bas"></div>
                    <div class="line5-bas"></div>
                    <div class="line6-bas"></div> 
                    <div class="line7-bas"></div> 
                    <div class="line8-bas"></div>   

                </div>


            </div>

            <div id="trapezoid-bas">



            </div>

            <div id="trapezoid2-bas">



            </div>

        </section>


    </body>


</html>